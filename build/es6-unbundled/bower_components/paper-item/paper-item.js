import'@polymer/polymer/polymer-legacy.js';import'@polymer/iron-flex-layout/iron-flex-layout.js';import'./paper-item-shared-styles.js';import{Polymer}from'@polymer/polymer/lib/legacy/polymer-fn.js';import{html}from'@polymer/polymer/lib/utils/html-tag.js';import{PaperItemBehavior}from'./paper-item-behavior.js';Polymer({_template:html`
    <style include="paper-item-shared-styles">
      :host {
        @apply --layout-horizontal;
        @apply --layout-center;
        @apply --paper-font-subhead;

        @apply --paper-item;
      }
    </style>
    <slot></slot>
`,is:'paper-item',behaviors:[PaperItemBehavior]});